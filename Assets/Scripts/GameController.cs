﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public GameObject gameOverPanel;
    public GameObject pausePanel;
    public Text scoreText;
    public Text currentScoreText;
    public Text bestScoreText;

    PlayerController playerController;

    // Use this for initialization
    void Start()
    {
        playerController = GameObject.FindObjectOfType<PlayerController>();
        AdManager.Instance.ShowBanner();
    }
	
	// Update is called once per frame
	void Update () {
        scoreText.text = playerController.GetScore().ToString();

        if (Input.GetKeyDown(KeyCode.Escape) && !playerController.isGameOver)
        {
            pausePanel.SetActive(true);
            Time.timeScale = 0;
        }
    }

    public void GameOver()
    {
        Invoke("ShowGameOverPanel", 0.8f);
        AdManager.Instance.ShowVideo();
    }

    void ShowGameOverPanel()
    {
        scoreText.gameObject.SetActive(false);

        if (playerController.GetScore() > PlayerPrefs.GetInt("Best", 0))
        {
            PlayerPrefs.SetInt("Best", playerController.GetScore());
        }

        currentScoreText.text = "Score: " + playerController.GetScore();
        bestScoreText.text = "Best Score: " + PlayerPrefs.GetInt("Best", 0).ToString();

        gameOverPanel.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ReturnToMainMenu()
    {
        AdManager.Instance.RemoveBanner();
        SceneManager.LoadScene("MainMenu");
    }

    public void ResumeGame()
    {
        pausePanel.SetActive(false);
        Time.timeScale = playerController.time;
    }
}
