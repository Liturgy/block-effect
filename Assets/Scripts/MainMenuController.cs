﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {
    public Sprite bonusPassedImage;
    public Text bonusText;
    public Button bonusButton;
    public GameObject AdManager;

    // Use this for initialization
    void Start () {
        bonusButton = bonusButton.GetComponent<Button>();
    }
	
	// Update is called once per frame
	void Update () {

        if (PlayerPrefs.GetInt("Best", 0) >= 50)
        {
            bonusButton.GetComponent<Image>().sprite = bonusPassedImage;
            bonusButton.interactable = true;
            bonusText.text = "Congratulations, enjoy your gift";
        }
    }

    public void StartPress()
    {
        SceneManager.LoadScene("Main");
    }

    public void HowToPlayPress()
    {
        SceneManager.LoadScene("HowToPlay");
    }
    public void QuitPress()
    {
        Application.Quit();
    }

    public void BonusPress()
    {
        SceneManager.LoadScene("Bonus");
    }
}
