﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour {

    public float scrollSpeed = 5.0f;

    const float BLOCK_START_HEIGHT = 45.25f;
    const float BLOCK_END_HEIGHT = -20.75f;
    const float DEFAULT_SCROLL_SPEED = 5.0f;

    public Vector3 setColumnStartHeight;

    // Use this for initialization
    void Start() {

        setColumnStartHeight = new Vector3(0, Random.Range(-23, 0), 0);
        transform.position = setColumnStartHeight;        
    }

    // Update is called once per frame
    void Update() {
        
    }

    private void FixedUpdate()
    {
        ScrollBlocks();
    }

    void ScrollBlocks()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject currentChild = transform.GetChild(i).gameObject;
            Vector2 changeHeight = currentChild.transform.position;

            currentChild.transform.Translate(Vector2.down * scrollSpeed * Time.deltaTime);

            if (currentChild.transform.position.y < BLOCK_END_HEIGHT)
            {
                changeHeight.y = BLOCK_START_HEIGHT;
                currentChild.transform.position = changeHeight;
            }
        }
    }
}

