﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using admob;

public class AdManager : MonoBehaviour {

    public static AdManager Instance { set; get;}

    void Start () {
        Instance = this;
        DontDestroyOnLoad(gameObject);

        Admob.Instance().initAdmob("ca-app-pub-5900652901228757/4862840222", "ca-app-pub-5900652901228757/6339573421");

        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
    }

    public void ShowBanner()
    {
        Admob.Instance().showBannerRelative(AdSize.Banner, AdPosition.TOP_CENTER, 5);
    }

    public void RemoveBanner()
    {
        Admob.Instance().removeBanner();
    }

    public void ShowVideo()
    {
        if (Admob.Instance().isInterstitialReady())
        {
            Admob.Instance().showInterstitial();
        }
        else
        {
            Admob.Instance().loadInterstitial();
        }
    }
}
