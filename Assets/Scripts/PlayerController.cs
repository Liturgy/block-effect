﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float thrust = 18.0f;
    public GameObject playerClone;
    public GameObject tapArea;
    public float time = 1.0f;
    public PhysicsMaterial2D bouncy;
    public PhysicsMaterial2D veryBouncy;
    public bool isGameOver = false;

    GameController gameController;

    Rigidbody2D rb2d;
    SpriteRenderer playerColor;
    Color boxColor;
    Color defaultColor;
    string appliedColor;
    Vector3 playerStartingPosition;
    Vector3 tapAreaStartingPosition;
    GameObject clone;
    float clickRate = 0.5f;
    float nextClick = 0.0f;
    bool hasGameStarted = false;
    bool playerJump = false;
    bool switchSide = false;
    bool isColorApplied = false;
    bool isPlayerInTapArea = false;
    bool isPlayerAllowedToTap = true;
    static int score;


    // Use this for initialization
    void Start () {

        rb2d = GetComponent<Rigidbody2D>();
        gameController = GameObject.FindObjectOfType<GameController>();
        playerStartingPosition = transform.position;
        tapAreaStartingPosition = tapArea.transform.position;
        playerColor = transform.GetChild(1).GetComponent<SpriteRenderer>();
        defaultColor = transform.GetChild(1).GetComponent<SpriteRenderer>().color;
        score = 0;

        Time.timeScale = 0;

        int randomInt = Random.Range(0, 2);
        
        if (randomInt == 0)
        {
            switchSide = true;
        }
	}

    // Update is called once per frame
    void Update() {

        if (!hasGameStarted && Input.GetMouseButtonDown(0))
        {
            Time.timeScale = time;
            hasGameStarted = true;
        }

        if (transform.position.y < -5.565 || (clone && clone.transform.position.y < -5.565))
        {
            if (appliedColor == "RedBlock")
            {
                ResetPlayerColor();
                ClearProperties();
                transform.position = playerStartingPosition;
                rb2d.velocity = new Vector2(0, -5);
                appliedColor = "WhiteBlock";
            }
            else
            {
                GameOver();
            }
        }

        if (Time.time > nextClick)
        {
            nextClick = Time.time + clickRate;
            isPlayerAllowedToTap = true;
        }

        if (Input.GetMouseButtonDown(0) && isPlayerInTapArea && isPlayerAllowedToTap)
        {
            if (appliedColor == "RedBlock")
            {
                GameOver();
            }

            nextClick = Time.time + clickRate;
            playerJump = true;
            isPlayerAllowedToTap = false;
        }
        else if (Input.GetMouseButtonDown(0) && !clone && Time.time > 0)
        {
            nextClick = Time.time + clickRate;
            isPlayerAllowedToTap = false;
        }
        
        if (isPlayerAllowedToTap)
        {
            tapArea.gameObject.GetComponent<SpriteRenderer>().color = Color.green;
        }
        else
        {
            tapArea.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        }

        if (clone && clone.transform.position.y < -3.0 && Input.GetMouseButtonDown(0))
        {
            Destroy(clone);
        }
    }

    void FixedUpdate() 
    {
        if (playerJump && !isGameOver)
        {           
            MakePlayerJump(rb2d);
            ApplyChallenge(appliedColor);
            
            switchSide = !switchSide;
            playerJump = false;         
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        string blockTag = collision.gameObject.tag;
        
        if (blockTag != "PlayerClone" && !isColorApplied || (blockTag == "WhiteBlock" && !isPlayerInTapArea))
        {           
            boxColor = collision.gameObject.GetComponent<SpriteRenderer>().color;
            playerColor.color = boxColor;
            appliedColor = blockTag;
            isColorApplied = true;
        }   
        
        if (blockTag == "WhiteBlock")
        {
            ClearProperties();
        }          
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isPlayerInTapArea = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isPlayerInTapArea = false;
    }

    void ApplyChallenge(string appliedColor)
    {
        score++;
        ClearProperties();

        switch (appliedColor)
        {
            case "RedBlock":
                break;

            case "YellowBlock":
                var fastTime = time + 0.7f;
                Time.timeScale = fastTime;
                break;

            case "GreenBlock":
                clone = Instantiate(playerClone, transform.position, transform.rotation);
                MakePlayerJump(clone.GetComponent<Rigidbody2D>());             
                break;

            case "BlueBlock":
                Vector3 newTapAreaPosition = tapAreaStartingPosition;
                newTapAreaPosition.y = tapAreaStartingPosition.y - 0.8f;
                tapArea.transform.position = newTapAreaPosition;             
                break;

            case "IndigoBlock":
                Camera.main.transform.rotation = Quaternion.Euler(0, 0, 180);
                break;
        
            case "VioletBlock":
                transform.localScale = new Vector3(2, 2, 2);
                transform.GetComponent<BoxCollider2D>().sharedMaterial = veryBouncy;
                break;

            case "WhiteBlock":
                ClearProperties();                
                break;
        }

        if (!isGameOver)
        {
            isColorApplied = false;
            ResetPlayerColor();
        }
    }

    void ResetPlayerColor()
    {
        playerColor.color = defaultColor;
    }

    void GameOver()
    {
        if (!isGameOver)
        {
            isGameOver = true;
            transform.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            gameController.GameOver();
        }
    }

    void ClearProperties()
    {
        appliedColor = "WhiteBlock";
        Time.timeScale = time;
        transform.localScale = new Vector3(1, 1, 1);
        transform.GetComponent<BoxCollider2D>().sharedMaterial = bouncy;
        tapArea.transform.position = tapAreaStartingPosition;
        Camera.main.transform.rotation = Quaternion.Euler(0, 0, 0);
    }
        
    void MakePlayerJump(Rigidbody2D rb2d)
    {
        float randomLeftValue = Random.Range(-8.0f, -1.0f);
        float randomRightValue = Random.Range(1.0f, 8.0f);

        if (switchSide)
        {
            rb2d.velocity = new Vector3(randomLeftValue, thrust, 0);
        }
        else
        {
            rb2d.velocity = new Vector3(randomRightValue, thrust, 0);
        }

        rb2d.AddTorque(5);
        time = time + 0.01f;
    }

    public int GetScore()
    {
        return score;
    }
}
